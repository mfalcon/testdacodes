package com.mlfl.movieapp.model;

public class Movies {
    private int id;
    private float popularity;
    private String poster_path;
    private String title;
    private String vote_average;
    private String release_date;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public float getPopularity(){
        return popularity;
    }

    public void setPopularity(float popularity){
        this.popularity = popularity;
    }

    public String getPosterpath(){
        return poster_path;
    }

    public void setPosterpath(String poster_path){
        this.poster_path = poster_path;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getVoteAverage(){
        return vote_average;
    }

    public void setVoteAverage(String vote_average){
        this.vote_average = vote_average;
    }

    public String getReleaseDate(){
        return release_date;
    }

    public void setReleaseDate(String release_date){
        this.release_date = release_date;
    }
}

package com.mlfl.movieapp.model;

import java.util.ArrayList;

public class ImagesConfig {
    private String base_url;
    private String secure_base_url;
    private ArrayList<String> backdrop_sizes;
    private ArrayList<String> logo_sizes;
    private ArrayList<String> poster_sizes;

    public String getBaseUrl(){
        return base_url;
    }

    public void setBaseUrl(String base_url){
        this.base_url = base_url;
    }

    public String getSecureBaseUrl(){
        return secure_base_url;
    }

    public void setSecureBaseUrl(String secure_base_url){
        this.secure_base_url = secure_base_url;
    }

    public ArrayList<String> getBackdropSizes(){
        return backdrop_sizes;
    }

    public void setBackdropSizes(ArrayList<String> backdrop_sizes){
        this.backdrop_sizes = backdrop_sizes;
    }

    public ArrayList<String> getLogoSizes(){
        return logo_sizes;
    }

    public void setLogoSizes(ArrayList<String> logo_sizes){
        this.logo_sizes = logo_sizes;
    }

    public ArrayList<String> getPosterSizes(){
        return poster_sizes;
    }

    public void setPosterSizes(ArrayList<String> poster_sizes){
        this.poster_sizes = poster_sizes;
    }

}

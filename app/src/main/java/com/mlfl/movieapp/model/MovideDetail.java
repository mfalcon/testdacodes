package com.mlfl.movieapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class MovideDetail implements Serializable {
    private String title;
    private String backdrop_path;
    private String release_date;
    private String runtime;
    private String vote_average;
    private String overview;
    private ArrayList<MovieDetailGenre> genres;

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getBackdropsPath(){
        return backdrop_path;
    }

    public void setBackDropsPath(String backdrop_path){
        this.backdrop_path = backdrop_path;
    }

    public String getReleaseDate(){
        return release_date;
    }

    public void setReleaseDate(String release_date){
        this.release_date = release_date;
    }

    public String getRuntime(){
        return runtime;
    }

    public void setRuntime(String runtime){
        this.runtime = runtime;
    }

    public String getVoteAverage(){
        return vote_average;
    }

    public void setVoteAverage(){
        this.vote_average = vote_average;
    }

    public String getOverview(){
        return overview;
    }

    public void setOverview(String overview){
        this.overview = overview;
    }

    public ArrayList<MovieDetailGenre> getGenres(){
        return genres;
    }

    public void setGenres(ArrayList<MovieDetailGenre> genres){
        this.genres = genres;
    }
}

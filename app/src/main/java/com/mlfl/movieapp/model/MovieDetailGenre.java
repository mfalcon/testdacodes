package com.mlfl.movieapp.model;

import java.io.Serializable;

public class MovieDetailGenre implements Serializable {
    private String name;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}

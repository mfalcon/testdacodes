package com.mlfl.movieapp.model;

public class Images {
    private ImagesConfig images;

    public ImagesConfig getImagesConfig(){
        return images;
    }

    public void setImagesConfig(ImagesConfig imagesConfig){
        this.images = imagesConfig;
    }
}

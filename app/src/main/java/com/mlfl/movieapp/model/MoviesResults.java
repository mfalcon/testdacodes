package com.mlfl.movieapp.model;

import java.util.ArrayList;

public class MoviesResults {
    private ArrayList<Movies> results;
    private String page;
    private String total_pages;

    public ArrayList<Movies> getResults() {
        return results;
    }

    public void setResults(ArrayList<Movies> results){
        this.results = results;
    }

    public String getPage(){
        return page;
    }

    public void setPage(String page){
        this.page = page;
    }

    public String getTotalPages(){
        return total_pages;
    }

    public void setTotalPages(String total_pages){
        this.total_pages = total_pages;
    }
}

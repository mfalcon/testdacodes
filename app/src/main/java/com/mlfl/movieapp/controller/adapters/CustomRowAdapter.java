package com.mlfl.movieapp.controller.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mlfl.movieapp.R;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomRowAdapter extends RecyclerView.Adapter<CustomRowAdapter.ViewHolder> {
    private ArrayList<HashMap<String,String>> detailMovie;
    private Context context;

    public CustomRowAdapter(Context context, ArrayList<HashMap<String, String>> detailMovie){
        this.detailMovie = detailMovie;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtMovieTitle.setText(detailMovie.get(position).get("title"));
        holder.txtMovieSubtitle.setText(detailMovie.get(position).get("subtitle"));
    }

    @Override
    public int getItemCount() {
        return detailMovie.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtMovieTitle, txtMovieSubtitle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtMovieTitle = itemView.findViewById(R.id.txtMovieTitle);
            txtMovieSubtitle = itemView.findViewById(R.id.txtMovieSubtitle);

        }
    }
}

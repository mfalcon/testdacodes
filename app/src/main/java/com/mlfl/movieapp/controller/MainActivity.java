package com.mlfl.movieapp.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.widget.Toast;

import com.mlfl.movieapp.R;
import com.mlfl.movieapp.controller.adapters.CustomGridAdapter;
import com.mlfl.movieapp.io.MovieAppApiAdapter;
import com.mlfl.movieapp.model.Images;
import com.mlfl.movieapp.model.Movies;
import com.mlfl.movieapp.model.MoviesResults;
import com.mlfl.movieapp.utils.ColumnItemDecoration;
import com.mlfl.movieapp.utils.Constants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView movieListRV;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<Movies> movies;
    Constants objConstants = new Constants();
    CustomGridAdapter customGridAdapter;
    GridLayoutManager gridLayoutManager;
    Images images;
    int page, totalPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        movieListRV = findViewById(R.id.movieListRV);
        swipeRefreshLayout = findViewById(R.id.swipeContainer);

        movies = new ArrayList<>();
        images = new Images();

        gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        movieListRV.addItemDecoration(new ColumnItemDecoration());
        movieListRV.setLayoutManager(gridLayoutManager);

        loadMovies();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                clear();
                loadMovies();
            }
        });

    }

    public void loadMovies(){
        //Call Configuration Service
        Call<Images> callConfiguration = MovieAppApiAdapter.getApiService().getConfiguration(objConstants.getApiKey());
        callConfiguration.enqueue(new Callback<Images>() {
            @Override
            public void onResponse(@NotNull Call<Images> callConfiguration, @NotNull Response<Images> response) {
                if(response.isSuccessful()){
                    images = response.body();

                    //Call Movie Service
                    Call<MoviesResults> call = MovieAppApiAdapter.getApiService().getMovieList(objConstants.getApiKey(), objConstants.getLanguage(), "1");
                    call.enqueue(new Callback<MoviesResults>() {
                        @Override
                        public void onResponse(@NotNull Call<MoviesResults> call, @NotNull Response<MoviesResults> response) {
                            if(response.isSuccessful()){
                                if(response.body() != null){
                                    movies = response.body().getResults();
                                    page = Integer.parseInt(response.body().getPage());
                                    totalPages = Integer.parseInt(response.body().getTotalPages());
                                    customGridAdapter = new CustomGridAdapter(getBaseContext(), getParent(), movies, images);

                                    movieListRV.setAdapter(customGridAdapter);
                                    movieListRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                        @Override
                                        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                            super.onScrollStateChanged(recyclerView, newState);
                                        }

                                        @Override
                                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                            super.onScrolled(recyclerView, dx, dy);
                                            if(gridLayoutManager != null && gridLayoutManager.findLastCompletelyVisibleItemPosition() == movies.size() - 1){
                                                if(page <= totalPages){
                                                    loadingMore();
                                                } else {
                                                    Toast.makeText(getApplicationContext(),"No hay más películas para mostrar", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                    });
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NotNull Call<MoviesResults> call, @NotNull Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call<Images> callConfiguration, @NotNull Throwable t) {

            }
        });
    }

    //Function to load more items
    public void loadingMore(){
        //Call Configuration Service
        Call<Images> callConfiguration = MovieAppApiAdapter.getApiService().getConfiguration(objConstants.getApiKey());
        callConfiguration.enqueue(new Callback<Images>() {
            @Override
            public void onResponse(@NotNull Call<Images> callConfiguration, @NotNull Response<Images> response) {
                if(response.isSuccessful()){
                    images = response.body();

                    //Call Movie Service
                    Call<MoviesResults> call = MovieAppApiAdapter.getApiService().getMovieList(objConstants.getApiKey(), objConstants.getLanguage(), String.valueOf(page + 1));
                    call.enqueue(new Callback<MoviesResults>() {
                        @Override
                        public void onResponse(@NotNull Call<MoviesResults> call, @NotNull Response<MoviesResults> response) {
                            if(response.isSuccessful()){
                                if(response.body() != null){
                                    page = Integer.parseInt(response.body().getPage());
                                    totalPages = Integer.parseInt(response.body().getTotalPages());
                                    movies.addAll(response.body().getResults());
                                    /*for(int i=0; i< response.body().getResults().size(); i++){
                                        movies.add(response.body().getResults().get(i));
                                    }*/
                                    int itemCount = customGridAdapter.getItemCount();
                                    customGridAdapter.notifyItemRangeInserted(itemCount, movies.size());
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NotNull Call<MoviesResults> call, @NotNull Throwable t) {

                        }
                    });
                }
            }

            @Override
            public void onFailure(@NotNull Call<Images> callConfiguration, @NotNull Throwable t) {

            }
        });
    }

    public void clear(){
        movies.clear();
        page = 0;
        totalPages = 0;
    }
}
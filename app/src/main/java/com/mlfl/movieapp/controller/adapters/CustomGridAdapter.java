package com.mlfl.movieapp.controller.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mlfl.movieapp.R;
import com.mlfl.movieapp.controller.DetailActivity;
import com.mlfl.movieapp.io.MovieAppApiAdapter;
import com.mlfl.movieapp.model.Images;
import com.mlfl.movieapp.model.MovideDetail;
import com.mlfl.movieapp.model.Movies;
import com.mlfl.movieapp.utils.Constants;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomGridAdapter extends RecyclerView.Adapter<CustomGridAdapter.ViewHolder> {
    private final Context context;
    private final Activity activity;
    ArrayList<Movies> moviesList;
    MovideDetail movideDetail;
    Images images;
    String secureUrl;
    String datOutput;

    public CustomGridAdapter(Context context, Activity activity, ArrayList<Movies> moviesList, Images images){
        this.context = context;
        this.activity = activity;
        this.moviesList = moviesList;
        this.images = images;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_grid_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Constants objConstants = new Constants();

        String newDate = objConstants.changeDate(moviesList.get(position).getReleaseDate(), datOutput);

        holder.txtTitle.setText(moviesList.get(position).getTitle());
        holder.txtReleaseDate.setText(newDate);
        holder.txtVoteAverage.setText(moviesList.get(position).getVoteAverage());

        secureUrl = images.getImagesConfig().getSecureBaseUrl();
        String size = images.getImagesConfig().getPosterSizes().get(4);
        String posterFullUrl  = secureUrl + size + moviesList.get(position).getPosterpath();
        Picasso.get().load(posterFullUrl).into(holder.imgPoster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movideDetail = new MovideDetail();
                String urlMovieDetail = objConstants.baseUrl() + "movie/" + moviesList.get(position).getId();
                Call<MovideDetail> call = MovieAppApiAdapter.getApiService().getMovieDetail(urlMovieDetail, objConstants.getApiKey(), objConstants.getLanguage());
                call.enqueue(new Callback<MovideDetail>() {
                    @Override
                    public void onResponse(@NotNull Call<MovideDetail> call, @NotNull Response<MovideDetail> response) {
                        if(response.isSuccessful()){
                            if(response.body() != null){
                                movideDetail = response.body();
                                Intent intent = new Intent(context, DetailActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("DetailMovie", movideDetail);
                                context.startActivity(intent);
                            } else {
                                Toast.makeText(context,"Algo salio, mal por favor espere un momento", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context,"Algo salio, mal por favor espere un momento", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<MovideDetail> call, @NotNull Throwable t) {
                        Toast.makeText(context,"Algo salio, mal por favor espere un momento", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtTitle, txtVoteAverage, txtReleaseDate;
        private ImageView imgPoster;

        public ViewHolder(@NonNull View itemView){
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtVoteAverage = itemView.findViewById(R.id.txtVoteAverage);
            txtReleaseDate = itemView.findViewById(R.id.txtReleaseDate);
            imgPoster = itemView.findViewById(R.id.posterImage);
        }
    }
}

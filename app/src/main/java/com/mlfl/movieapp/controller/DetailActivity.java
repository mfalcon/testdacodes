package com.mlfl.movieapp.controller;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mlfl.movieapp.R;
import com.mlfl.movieapp.controller.adapters.CustomRowAdapter;
import com.mlfl.movieapp.io.MovieAppApiAdapter;
import com.mlfl.movieapp.model.Images;
import com.mlfl.movieapp.model.MovideDetail;
import com.mlfl.movieapp.utils.Constants;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {
    ImageView posterImage;
    TextView txtTitle;
    Images images;
    RecyclerView detailListRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        images = new Images();
        detailListRV = findViewById(R.id.detailListRV);
        posterImage = findViewById(R.id.detailPosterImage);
        txtTitle = findViewById(R.id.txtTitleDetail);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Constants objConstants = new Constants();
        MovideDetail movideDetail = (MovideDetail) getIntent().getExtras().getSerializable("DetailMovie");
        Call<Images> call = MovieAppApiAdapter.getApiService().getConfiguration(objConstants.getApiKey());
        call.enqueue(new Callback<Images>() {
            @Override
            public void onResponse(@NotNull Call<Images> call, @NotNull Response<Images> response) {

                ArrayList<HashMap<String,String>> arrayListDetail = new ArrayList<>();

                if(response.isSuccessful()){
                    if(response.body() != null){
                        images = response.body();
                        String secureUrl = images.getImagesConfig().getSecureBaseUrl();
                        String size = images.getImagesConfig().getBackdropSizes().get(2);
                        String posterFullUrl  = secureUrl + size + movideDetail.getBackdropsPath();
                        Picasso.get().load(posterFullUrl).into(posterImage);

                        txtTitle.setText(movideDetail.getTitle());


                        HashMap<String,String> mapRuntime = new HashMap<>();
                        mapRuntime.put("title", "Duración:");
                        mapRuntime.put("subtitle", movideDetail.getRuntime() + " min");
                        arrayListDetail.add(mapRuntime);

                        HashMap<String,String> mapReleaseDate = new HashMap<>();
                        mapReleaseDate.put("title","Fecha de estreno:");
                        mapReleaseDate.put("subtitle", movideDetail.getReleaseDate());
                        arrayListDetail.add(mapReleaseDate);

                        HashMap<String,String> mapVoteAverage = new HashMap<>();
                        mapVoteAverage.put("title","Calificación:");
                        mapVoteAverage.put("subtitle", movideDetail.getVoteAverage());
                        arrayListDetail.add(mapVoteAverage);

                        HashMap<String, String> mapCalificacion = new HashMap<>();
                        String calificacion = "";
                        for(int i=0; i<movideDetail.getGenres().size(); i++){
                            calificacion = calificacion + "  " +  movideDetail.getGenres().get(i).getName();
                        }
                        mapCalificacion.put("title", "Calificación");
                        mapCalificacion.put("subtitle", calificacion);
                        arrayListDetail.add(mapCalificacion);

                        HashMap<String,String> mapOverview = new HashMap<>();
                        mapOverview.put("title","Descripción:");
                        mapOverview.put("subtitle", movideDetail.getOverview());
                        arrayListDetail.add(mapOverview);

                        CustomRowAdapter customRowAdapter = new CustomRowAdapter(getBaseContext(), arrayListDetail);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        detailListRV.setLayoutManager(layoutManager);
                        detailListRV.setAdapter(customRowAdapter);
                    } else {
                        Toast.makeText(getApplicationContext(),"Algo salio, mal por favor espere un momento", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),"Algo salio, mal por favor espere un momento", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<Images> call, @NotNull Throwable t) {
                Toast.makeText(getApplicationContext(),"Algo salio, mal por favor espere un momento", Toast.LENGTH_SHORT).show();
            }
        });

    }

    //Manage back button
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}
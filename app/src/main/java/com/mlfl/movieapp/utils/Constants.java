package com.mlfl.movieapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Constants {
    String url;
    final String apiKey;
    String language;

    public Constants(){
        url = "https://api.themoviedb.org/3/";
        apiKey = "a28c4bc831b590dc669ef8a459fdbff7";
        language = "es-MX";
    }

    public String baseUrl(){
        return url;
    }

    public String getApiKey(){
        return apiKey;
    }

    public String getLanguage(){
        return language;
    }

    //Function for parsing dates with an specific format
    public String changeDate(String date, String dateOutput){
        try{
            SimpleDateFormat inputDate = new SimpleDateFormat("yyyy-MM-dd");
            Date dateValue = inputDate.parse(date);
            SimpleDateFormat outputDate = new SimpleDateFormat("dd-MMM-yyyy");
            dateOutput = outputDate.format(dateValue);
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return dateOutput;
    }
}

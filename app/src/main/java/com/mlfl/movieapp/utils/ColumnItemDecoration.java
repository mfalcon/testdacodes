package com.mlfl.movieapp.utils;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ColumnItemDecoration extends RecyclerView.ItemDecoration {
    // Horizontal padding
    private final int padding;

    public ColumnItemDecoration() {
        // Set padding (from resources probably)
        padding = 15;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();
        GridLayoutManager gridLayoutManager = (GridLayoutManager) parent.getLayoutManager();
        int position = parent.getChildViewHolder(view).getAdapterPosition();
        float spanSize = gridLayoutManager.getSpanSizeLookup().getSpanSize(position);
        float totalSpanSize = gridLayoutManager.getSpanCount();

        float n = totalSpanSize / spanSize;
        float c = layoutParams.getSpanIndex() / spanSize;

        float leftPadding = padding * ((n - c) / n);
        float rightPadding = padding * ((c + 1) / n);

        outRect.left = (int) leftPadding;
        outRect.right = (int) rightPadding;
    }
}

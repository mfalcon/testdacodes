package com.mlfl.movieapp.io;

import com.mlfl.movieapp.model.Images;
import com.mlfl.movieapp.model.MovideDetail;
import com.mlfl.movieapp.model.MoviesResults;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface MovieAppApiService {
    //Movie List Service
    @GET("movie/now_playing")
    Call<MoviesResults> getMovieList(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") String page);

    //Configuration Service
    @GET("configuration")
    Call<Images> getConfiguration(@Query("api_key") String apiKey);

    //Movie Detail
    @GET
    Call<MovideDetail> getMovieDetail(@Url String url, @Query("api_key") String apiKey, @Query("language") String language);
}

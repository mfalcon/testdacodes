package com.mlfl.movieapp.io;
import com.mlfl.movieapp.utils.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieAppApiAdapter {
    private static Constants objConstants = new Constants();
    private static MovieAppApiService API_SERVICE;

    public static MovieAppApiService getApiService(){
        //Activity monitoring
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //Interceptor and requests
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        String baseUrl = objConstants.baseUrl();

        if(API_SERVICE == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
            API_SERVICE = retrofit.create(MovieAppApiService.class);
        }
        return API_SERVICE;
    }
}
